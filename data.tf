data "azuread_client_config" "current" {}

data "azurerm_kubernetes_cluster" "aks_cluster" {
  name                = var.cluster_name
  resource_group_name = var.cluster_rg
}

## Overview
This is a deployment of Element-Web with Matrix backend using Terraform to an AKS cluster.


## Providers

| Name | Version |
|------|---------|
| <a name="provider_azuread"></a> [azuread](#provider\_azuread) | n/a |
| <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) | n/a |
| <a name="provider_azurerm.identity"></a> [azurerm.identity](#provider\_azurerm.identity) | n/a |
| <a name="provider_helm"></a> [helm](#provider\_helm) | n/a |
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | n/a |
| <a name="provider_random"></a> [random](#provider\_random) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [azuread_application.element](https://registry.terraform.io/providers/hashicorp/azuread/latest/docs/resources/application) | resource |
| [azuread_application_password.element](https://registry.terraform.io/providers/hashicorp/azuread/latest/docs/resources/application_password) | resource |
| [azuread_service_principal.element](https://registry.terraform.io/providers/hashicorp/azuread/latest/docs/resources/service_principal) | resource |
| [azurerm_private_dns_a_record.element](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/private_dns_a_record) | resource |
| [azurerm_private_dns_a_record.matrix](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/private_dns_a_record) | resource |
| [helm_release.matrix](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [kubernetes_config_map.element-config](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/config_map) | resource |
| [kubernetes_deployment.element](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/deployment) | resource |
| [kubernetes_ingress_v1.element](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/ingress_v1) | resource |
| [kubernetes_manifest.certificate_element_tls_letsencrypt](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/manifest) | resource |
| [kubernetes_manifest.certificate_matrix_tls_letsencrypt](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/manifest) | resource |
| [kubernetes_namespace.element-web](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/namespace) | resource |
| [kubernetes_namespace.matrix](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/namespace) | resource |
| [kubernetes_service.element](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/service) | resource |
| [random_uuid.element_scope_id](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/uuid) | resource |
| [azuread_client_config.current](https://registry.terraform.io/providers/hashicorp/azuread/latest/docs/data-sources/client_config) | data source |
| [azurerm_kubernetes_cluster.aks_cluster](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/data-sources/kubernetes_cluster) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cluster_name"></a> [cluster\_name](#input\_cluster\_name) | Cluster to deploy element | `string` | n/a | yes |
| <a name="input_cluster_rg"></a> [cluster\_rg](#input\_cluster\_rg) | Cluster RG name to deploy Mattermost | `string` | n/a | yes |
| <a name="input_default_tags"></a> [default\_tags](#input\_default\_tags) | A map of tags to add to all resources | `map(string)` | n/a | yes |
| <a name="input_dns_a_record_name_element"></a> [dns\_a\_record\_name\_element](#input\_dns\_a\_record\_name\_element) | The name of the A record to add to dns to access the website | `string` | n/a | yes |
| <a name="input_dns_a_record_name_matrix"></a> [dns\_a\_record\_name\_matrix](#input\_dns\_a\_record\_name\_matrix) | The name of the A record to add to dns to access the website | `string` | n/a | yes |
| <a name="input_dns_a_record_name_ttl"></a> [dns\_a\_record\_name\_ttl](#input\_dns\_a\_record\_name\_ttl) | ttl for dns record | `number` | n/a | yes |
| <a name="input_dns_zone_name_a_record"></a> [dns\_zone\_name\_a\_record](#input\_dns\_zone\_name\_a\_record) | DNS Zone to place A Records | `string` | n/a | yes |
| <a name="input_dns_zone_name_rg"></a> [dns\_zone\_name\_rg](#input\_dns\_zone\_name\_rg) | The name of the resource group the Azure DNS Zone for GitLab is located in | `string` | n/a | yes |
| <a name="input_element_ns"></a> [element\_ns](#input\_element\_ns) | K8s namespace to deploy element | `string` | n/a | yes |
| <a name="input_matrix_helm_version"></a> [matrix\_helm\_version](#input\_matrix\_helm\_version) | Version of the element Helm chart to install | `string` | n/a | yes |
| <a name="input_matrix_ns"></a> [matrix\_ns](#input\_matrix\_ns) | K8s namespace to deploy matrix | `string` | n/a | yes |
| <a name="input_postgres_password"></a> [postgres\_password](#input\_postgres\_password) | Postgres Password for Matrix | `string` | n/a | yes |
| <a name="input_redis_password"></a> [redis\_password](#input\_redis\_password) | Redis Password for Matrix | `string` | n/a | yes |
| <a name="input_subscription_id"></a> [subscription\_id](#input\_subscription\_id) | Subscription ID of the Azure subscription | `string` | n/a | yes |
| <a name="input_tenant_id"></a> [tenant\_id](#input\_tenant\_id) | Tenant ID of the Azure account | `string` | n/a | yes |

## Outputs

No outputs.

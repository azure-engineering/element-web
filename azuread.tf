resource "azuread_application" "element" {
  display_name     = "Element"
  owners           = [data.azuread_client_config.current.object_id]
  sign_in_audience = "AzureADandPersonalMicrosoftAccount"
  identifier_uris  = ["https://matrix.example.com"]
  group_membership_claims = [
    "SecurityGroup",
  ]

  web {
    redirect_uris = ["https://matrix.example.com/_synapse/client/oidc/callback"]
  }

  required_resource_access {
    resource_app_id = ""

    resource_access {
      id   = ""
      type = "Scope"
    }
    resource_access {
      id   = ""
      type = "Scope"
    }
    resource_access {
      id   = ""
      type = "Scope"
    }
  }

  api {
    known_client_applications      = []
    mapped_claims_enabled          = false
    requested_access_token_version = 2
  }

  timeouts {}

  optional_claims {
    id_token {
      additional_properties = []
      essential             = false
      name                  = "groups"
    }
    id_token {
      additional_properties = []
      essential             = false
      name                  = "email"
    }
  }

  app_role {
    allowed_member_types = ["User"]
    description          = "element-access"
    display_name         = "element-access"
    enabled              = true
    id                   = random_uuid.element_scope_id.result
    value                = "element-access"
  }
}

resource "azuread_service_principal" "element" {
  application_id               = azuread_application.element.application_id
  app_role_assignment_required = false
  owners                       = [data.azuread_client_config.current.object_id]

  feature_tags {
    custom_single_sign_on = false
    enterprise            = true
    gallery               = false
    hide                  = false
  }

  saml_single_sign_on {}

}

resource "azuread_application_password" "element" {
  application_object_id = azuread_application.element.object_id
  end_date_relative     = "17520h" #2 Years
  display_name          = "element-azuread-secret"
}

resource "random_uuid" "element_scope_id" {}

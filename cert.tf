resource "kubernetes_manifest" "certificate_matrix_tls_letsencrypt" {
  manifest = {
    "apiVersion" = "cert-manager.io/v1"
    "kind"       = "Certificate"
    "metadata" = {
      "name"      = "matrix-tls"
      "namespace" = "${kubernetes_namespace.matrix.metadata.0.name}"
    }
    "spec" = {
      "commonName" = "matrix.example.com"
      "dnsNames" = [
        "matrix.example.com",
      ]
      "issuerRef" = {
        "kind" = "ClusterIssuer"
        "name" = "letsencrypt-production"
      }
      "privateKey" = {
        "rotationPolicy" = "Always"
      }
      "secretName" = "matrix-tls"
      "usages" = [
        "digital signature",
        "key encipherment",
        "server auth",
      ]
    }
  }
  depends_on = [
    kubernetes_namespace.matrix,
  ]
}

resource "kubernetes_manifest" "certificate_element_tls_letsencrypt" {
  manifest = {
    "apiVersion" = "cert-manager.io/v1"
    "kind"       = "Certificate"
    "metadata" = {
      "name"      = "element-tls"
      "namespace" = "${kubernetes_namespace.element-web.metadata.0.name}"
    }
    "spec" = {
      "commonName" = "element.example.com"
      "dnsNames" = [
        "element.example.com",
      ]
      "issuerRef" = {
        "kind" = "ClusterIssuer"
        "name" = "letsencrypt-production"
      }
      "privateKey" = {
        "rotationPolicy" = "Always"
      }
      "secretName" = "element-tls"
      "usages" = [
        "digital signature",
        "key encipherment",
        "server auth",
      ]
    }
  }
  depends_on = [
    kubernetes_namespace.element-web,
  ]
}

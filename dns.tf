resource "azurerm_private_dns_a_record" "element" {
  name                = var.dns_a_record_name_element
  zone_name           = var.dns_zone_name_a_record
  resource_group_name = var.dns_zone_name_rg
  ttl                 = var.dns_a_record_name_ttl
  records             = [""]
  provider            = azurerm.identity

  depends_on = [
    kubernetes_ingress_v1.element
  ]

  tags = var.default_tags

}

resource "azurerm_private_dns_a_record" "matrix" {
  name                = var.dns_a_record_name_matrix
  zone_name           = var.dns_zone_name_a_record
  resource_group_name = var.dns_zone_name_rg
  ttl                 = var.dns_a_record_name_ttl
  records             = [""]
  provider            = azurerm.identity

  depends_on = [
    helm_release.matrix
  ]

  tags = var.default_tags

}

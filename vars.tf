## AZURE AD VARIABLES ----------------------------------------------------------

variable "tenant_id" {
  description = "Tenant ID of the Azure account"
  type        = string
}

variable "subscription_id" {
  description = "Subscription ID of the Azure subscription"
  type        = string
}

## KUBERNETES VARIABLES ----------------------------------------------------------

variable "cluster_rg" {
  description = "Cluster RG name to deploy Mattermost"
  type        = string
}

variable "cluster_name" {
  description = "Cluster to deploy element"
  type        = string
}

variable "element_ns" {
  description = "K8s namespace to deploy element"
  type        = string
}

variable "matrix_ns" {
  description = "K8s namespace to deploy matrix"
  type        = string
}

variable "redis_password" {
  description = "Redis Password for Matrix"
  type        = string
}

variable "postgres_password" {
  description = "Postgres Password for Matrix"
  type        = string
}

variable "matrix_helm_version" {
  description = "Version of the element Helm chart to install"
  type        = string
}

## DNS VARIABLES -------------------------------------------------------------------------

variable "dns_a_record_name_matrix" {
  description = "The name of the A record to add to dns to access the website"
  type        = string
}

variable "dns_a_record_name_element" {
  description = "The name of the A record to add to dns to access the website"
  type        = string
}

variable "dns_zone_name_rg" {
  description = "The name of the resource group the Azure DNS Zone for GitLab is located in"
  type        = string
}

variable "dns_zone_name_a_record" {
  description = "DNS Zone to place A Records"
  type        = string
}

variable "dns_a_record_name_ttl" {
  description = "ttl for dns record"
  type        = number
}

## TAGS ------------------------------------------------------------------------------------

variable "default_tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
}

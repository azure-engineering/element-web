resource "kubernetes_namespace" "element-web" {
  metadata {
    name = var.element_ns
  }
}

resource "kubernetes_config_map" "element-config" {
  metadata {
    name      = "element-config"
    namespace = kubernetes_namespace.element-web.metadata.0.name
  }

  data = {
    "config.json" = file("${path.module}/values/element.json")
  }
}

resource "kubernetes_deployment" "element" {
  metadata {
    name      = "element"
    namespace = kubernetes_namespace.element-web.metadata.0.name
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "element"
      }
    }

    template {
      metadata {
        labels = {
          app = "element"
        }
      }

      spec {
        container {
          image = "vectorim/element-web:latest"
          name  = "element"

          port {
            container_port = 80
            name           = "element"
          }

          volume_mount {
            mount_path = "/app/config.json"
            name       = "config-volume"
            sub_path   = "config.json"
          }

          readiness_probe {
            http_get {
              path = "/"
              port = "element"
            }

            initial_delay_seconds = 2
            period_seconds        = 3
          }

          liveness_probe {
            http_get {
              path = "/"
              port = "element"
            }

            initial_delay_seconds = 10
            period_seconds        = 10
          }
        }

        volume {
          name = "config-volume"

          config_map {
            name = kubernetes_config_map.element-config.metadata.0.name
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "element" {
  metadata {
    name      = "element"
    namespace = kubernetes_namespace.element-web.metadata.0.name
  }

  spec {
    selector = {
      app = kubernetes_deployment.element.metadata.0.name
    }

    port {
      port        = 80
      target_port = 80
    }
  }
}

resource "kubernetes_ingress_v1" "element" {
  metadata {
    name      = "element"
    namespace = kubernetes_namespace.element-web.metadata.0.name

    labels = {
      "app.kubernetes.io/instance" = "element"
      "app.kubernetes.io/name"     = "element-io"
    }

    annotations = {
      "nginx.ingress.kubernetes.io/configuration-snippet" = <<-EOT
        add_header X-Frame-Options SAMEORIGIN;
        add_header X-Content-Type-Options nosniff;
        add_header X-XSS-Protection "1; mode=block";
        add_header Content-Security-Policy "frame-ancestors 'self'";
      EOT
    }
  }

  spec {
    ingress_class_name = "nginx"
    tls {
      hosts       = ["element.example.com"]
      secret_name = "element-tls"
    }
    rule {
      host = "element.example.com"
      http {
        path {
          path      = "/"
          path_type = "Prefix"
          backend {
            service {
              name = "element"
              port {
                number = 80
              }
            }
          }
        }
      }
    }
  }
}

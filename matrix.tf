resource "kubernetes_namespace" "matrix" {
  metadata {
    name = var.matrix_ns
  }
}

resource "helm_release" "matrix" {
  name       = "matrix"
  namespace  = "matrix"
  repository = "https://ananace.gitlab.io/charts"
  chart      = "matrix-synapse"
  version    = "3.3.0"

  values = [templatefile("values/matrix.yaml", {
    client_secret = azuread_application_password.element.value
    client_id     = azuread_application.element.application_id
    tenant_id     = var.tenant_id
    redis_pass    = var.redis_password
    postgres_pass = var.postgres_password

  })]

}
